﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
   public class CharacterDTO
    {
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public int Pv { get; set; }
        public String HouseName { get; set; }
        public CharacterTypeEnum CharacterType { get; set; }

        public CharacterDTO(){}
    }
}