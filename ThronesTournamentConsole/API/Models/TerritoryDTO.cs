﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class TerritoryDTO
    {
        public EntitiesLayer.TerritoryType Type { get; set; }
        public string Owner { get; set; }
        public int id { get; set; }

        public TerritoryDTO() { }
    }
}