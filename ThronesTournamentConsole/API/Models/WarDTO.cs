﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class WarDTO
    {
        public List<EntitiesLayer.Fight> combats { get; set; }
        public String name { get; set; }
        public int id { get; set; }

        public WarDTO() { }
    }
}