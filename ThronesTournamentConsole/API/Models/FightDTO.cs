﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FightDTO
    {
        public string houseChallenger1 { get; set; }
        public string houseChallenger2 { get; set; }
        public string winningHouse { get; set; }
        public int id { get; set; }

        public FightDTO() { }

        public void AddWinningHouse(String wh)
        {
            winningHouse = wh;
        }
    }
}