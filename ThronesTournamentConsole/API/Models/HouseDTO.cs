﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class HouseDTO
    {
        public String Name { get; set; }
        public int NumberOfUnities { get; set; }

        public HouseDTO() { }
    }
}