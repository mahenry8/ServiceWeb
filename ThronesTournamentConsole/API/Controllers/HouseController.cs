﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace API.Controllers
{
    public class HouseController : ApiController
    {
        public List<HouseDTO> GetAllHouses()
        {
            List<HouseDTO> list = new List<HouseDTO>();
            DalManager m = DalManager.Instance;

            foreach (var house in m.ExistingHouses())
            {
                HouseDTO c = new HouseDTO();
                c.Name = house.Name;
                c.NumberOfUnities = house.NumberOfUnities;
                list.Add(c);
            }
            return list;
        }

        public HouseDTO GetHouse(String n)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.House house = m.GetHouseByName(n);
            HouseDTO c = new HouseDTO();
            c.Name = house.Name;
            c.NumberOfUnities = house.NumberOfUnities;
            return c;
        }

        public void AddHouse(HouseDTO h)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.House house = new EntitiesLayer.House(h.Name,h.NumberOfUnities);
            m.AddHouseInBDD(house);
        }
        public void DeleteHouse(HouseDTO h)
        {
            DalManager m = DalManager.Instance;
            m.DeleteHouseInBDD(h.Name);
        }
        public void PutHouse(HouseDTO h)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.House house = new EntitiesLayer.House(h.Name, h.NumberOfUnities);
            m.UpdateHouseInBDD(house);
        }
    }
}
