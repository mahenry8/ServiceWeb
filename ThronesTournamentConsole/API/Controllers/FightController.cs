﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class FightController : ApiController
    {
        public List<FightDTO> GetAllFights()
        {
            List<FightDTO> list = new List<FightDTO>();
            DalManager m = DalManager.Instance;

            foreach (var fig in m.ExistingFights())
            {
                FightDTO f = new FightDTO();
                f.id = fig.id;
                f.houseChallenger1 = fig.houseChallenger1;
                f.houseChallenger2 = fig.houseChallenger2;
                f.winningHouse = fig.winningHouse;
                list.Add(f);
            }
            return list;
        }

        public FightDTO GetFight(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Fight fig = m.GetFightById(id);
            FightDTO f = new FightDTO();
            f.id = fig.id;
            f.houseChallenger1 = fig.houseChallenger1;
            f.houseChallenger2 = fig.houseChallenger2;
            f.winningHouse = fig.winningHouse;
            return f;
        }

        public void AddFight(FightDTO f)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Fight fight = new EntitiesLayer.Fight(f.houseChallenger1, f.houseChallenger2, 1,2 );
            m.AddFightInBDD(fight);
        }
        public void DeleteFight(FightDTO f)
        {
            DalManager m = DalManager.Instance;
            m.DeleteFightInBDD(f.id);
        }
        public void PutFight(FightDTO f)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Fight fight = new EntitiesLayer.Fight(f.houseChallenger1, f.houseChallenger2, 1, 2);
            m.UpdateFightInBDD(fight);
        }
    }
}
