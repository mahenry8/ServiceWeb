﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace API.Controllers
{
    public class CharacterController : ApiController
    {
        public List<CharacterDTO> GetAllCharacters()
        {
            List<CharacterDTO> list = new List<CharacterDTO>();
            DalManager m = DalManager.Instance;

            foreach(var charac in m.ExistingCharacters())
            {
                CharacterDTO c = new CharacterDTO();
                c.Bravoury = charac.Bravoury;
                c.Crazyness = charac.Crazyness;
                c.FirstName = charac.FirstName;
                c.LastName = charac.LastName;
                c.Pv = charac.Pv;
                c.HouseName = charac.HouseName;
                c.CharacterType = charac.CharacterType;
                list.Add(c);
            }
            return list;
        }

        public CharacterDTO GetCharacter(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Character charac = m.GetCharacterById(id);
            CharacterDTO c = new CharacterDTO();
            c.Bravoury = charac.Bravoury;
            c.Crazyness = charac.Crazyness;
            c.FirstName = charac.FirstName;
            c.LastName = charac.LastName;
            c.Pv = charac.Pv;
            c.HouseName = charac.HouseName;
            c.CharacterType = charac.CharacterType;
            return c;
        }

        public void AddCharacter(CharacterDTO c)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Character charac = new EntitiesLayer.Character(0,c.Bravoury, c.Crazyness, c.FirstName, c.LastName,c.Pv,c.HouseName,c.CharacterType);
            m.AddCharacterInBDD(charac);
        }
        public void DeleteCharacter(CharacterDTO c)
        {
            DalManager m = DalManager.Instance;
            m.DeleteCharacterInBDD(c.FirstName, c.LastName);
        }
        public void PutCharacter(CharacterDTO c)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Character charac = new EntitiesLayer.Character(0, c.Bravoury, c.Crazyness, c.FirstName, c.LastName, c.Pv, c.HouseName, c.CharacterType);
            m.UpdateCharacterInBDD(charac);
        }
    }

}
