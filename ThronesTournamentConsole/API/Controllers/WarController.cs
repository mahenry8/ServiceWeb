﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace API.Controllers
{
    public class WarController : ApiController
    {
        public List<WarDTO> GetAllWars()
        {
            List<WarDTO> list = new List<WarDTO>();
            DalManager m = DalManager.Instance;

            foreach (var w in m.ExistingWars())
            {
                WarDTO war = new WarDTO();
                war.combats = w.combats;
                war.name = w.name;
                war.id = w.id;
                list.Add(war);
            }
            return list;
        }

        public WarDTO GetWar(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.War w = m.GetWarById(id);
            WarDTO war = new WarDTO();
            war.combats = w.combats;
            war.name = w.name;
            war.id = w.id;
            return war;
        }

        public void AddWar(WarDTO w)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.War war = new EntitiesLayer.War(w.id,w.name);
            m.AddWarInBDD(war);
        }
        public void DeleteWar(WarDTO w)
        {
            DalManager m = DalManager.Instance;
            m.DeleteWarInBDD(w.id);
        }
        public void PutWar(WarDTO w)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.War war = new EntitiesLayer.War(w.id, w.name);
            m.UpdateWarInBDD(war);
        }
    }
}
