﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class TerritoryController : ApiController
    {
        public List<TerritoryDTO> GetAllTerritories()
        {
            List<TerritoryDTO> list = new List<TerritoryDTO>();
            DalManager m = DalManager.Instance;

            foreach (var terr in m.ExistingTerritories())
            {
                TerritoryDTO t = new TerritoryDTO();
                t.id = terr.id;
                t.Type = terr.Type;
                t.Owner = terr.Owner;
                list.Add(t);
            }
            return list;
        }

        public TerritoryDTO GetTerritory(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Territory terr = m.GetTerritoryById(id);
            TerritoryDTO t = new TerritoryDTO();
            t.id = terr.id;
            t.Type = terr.Type;
            t.Owner = terr.Owner;
            return t;
        }
        public void AddTerritory(TerritoryDTO t)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Territory terr = new EntitiesLayer.Territory(t.Type, t.Owner, t.id);
            m.AddTerritoryInBDD(terr);
        }
        public void DeleteTerritory(TerritoryDTO t)
        {
            DalManager m = DalManager.Instance;
            m.DeleteTerritoryInBDD(t.id);
        }
        public void PutTerritory(TerritoryDTO c)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Territory terr = new EntitiesLayer.Territory(t.Type, t.Owner, t.id);
            m.UpdateTerritoryInBDD(terr);
        }
    }
}
