﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using System.Data;
using System.Data.SqlClient;
using EntitiesLayer;


namespace Tests
{
    [TestClass]
    public class DalManagerTest
    {
        private DalManager MyDal;
        [TestInitialize]
        public void Initialize()
        {
            MyDal = DalManager.Instance;
        }


        [TestMethod]
        public void Connection()
        {
            int rows = MyDal.SelectByDataAdapter("SELECT * FROM dbo.Territories;").Rows.Count;
            Assert.AreEqual(rows, 0);
        }

        [TestMethod]
        public void AddHouses()
        {
            House h1 = new House("Leet");
            House h2 = new House("Zero");
            MyDal.AddHouseInBDD(h1);
            MyDal.AddHouseInBDD(h2);

            int rows = MyDal.SelectByDataAdapter("SELECT * FROM dbo.Houses;").Rows.Count;
            Assert.AreEqual(rows, 2);
        }


        [TestMethod]
        public void AddChara()
        {
            Character c1 = new Character(42, 42, 42, "42", "1337", 42, "Leet", CharacterTypeEnum.LEADER);
            Character c2 = new Character(0, 0, 0, "très", "Null", 0, "Zero", CharacterTypeEnum.LOSER);
            MyDal.AddCharacterInBDD(c1);
            MyDal.AddCharacterInBDD(c2);

            int rows = MyDal.SelectByDataAdapter("SELECT * FROM dbo.Characters;").Rows.Count;
            Assert.AreEqual(rows, 2);
            
        }
        [TestMethod]
        public void AddTerritory()
        {
            Territory t1 = new Territory(TerritoryType.SEA, "Leet", 2);
            Territory t2 = new Territory(TerritoryType.MOUNTAIN, "Zero", 0);

            MyDal.AddTerritoryInBDD(t1);
            MyDal.AddTerritoryInBDD(t2);

            int rows = MyDal.SelectByDataAdapter("SELECT * FROM dbo.Territories;").Rows.Count;
            Assert.AreEqual(rows, 2);
        }

        [TestMethod]
        public void AddWar()
        {
            War w1 = new War(0,"Gueguerre");
            War w2 = new War(0, "GueguerreThe2");

            MyDal.AddWarInBDD(w1);
            MyDal.AddWarInBDD(w2);

            int rows = MyDal.SelectByDataAdapter("SELECT * FROM dbo.Wars;").Rows.Count;
            Assert.AreEqual(rows, 2);
        }

        [TestMethod]
        public void AddFight()
        {
            Fight f1 = new Fight("Leet", "Zero", 1, 0);
            Fight f2 = new Fight("Leet", "Zero", 2, 0);

            MyDal.AddFightInBDD(f1);
            MyDal.AddFightInBDD(f2);

            int rows = MyDal.SelectByDataAdapter("SELECT * FROM dbo.Fights;").Rows.Count;
            Assert.AreEqual(rows, 2);
        }

        [TestMethod]
        public void UpdateCharact()
        {
            Character c = new Character(42, 80, 42, "42", "1337", 42, "Leet", CharacterTypeEnum.LEADER);

            MyDal.UpdateCharacterInBDD(c);

            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Characters WHERE LastName='1337' AND FirstName='42';").Rows[0][1], 80);
        }

        [TestMethod]
        public void UpdateHouse()
        {
            House h = new House("Leet");
            h.NumberOfUnities=123;
            MyDal.UpdateHouseInBDD(h);

            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE Name='Leet';").Rows[0][1], 123);
        }
        // à finir à partir de là

        [TestMethod]
        public void UpdateTerritory()
        {
            int id_t = int.Parse(MyDal.SelectByDataAdapter("SELECT IdTerritory FROM dbo.Territories WHERE Type ='MOUNTAIN';").Rows[0][0].ToString());
            Territory t = new Territory(TerritoryType.SEA, "Zero", id_t);

            MyDal.UpdateTerritoryInBDD(t);

            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Territories WHERE Owner='Zero';").Rows[0][1].ToString(),"SEA" );
        }

        [TestMethod]
        public void UpdateFight()
        {
            Character c = new Character(42, 80, 42, "42", "1337", 42, "Leet", CharacterTypeEnum.LEADER);

            MyDal.UpdateCharacterInBDD(c);

            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Characters WHERE LastName='1337' AND FirstName='42';").Rows[0][1], 80);
        }

        [TestMethod]
        public void UpdateWar()
        {
            Character c = new Character(42, 80, 42, "42", "1337", 42, "Leet", CharacterTypeEnum.LEADER);

            MyDal.UpdateCharacterInBDD(c);

            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Characters WHERE LastName='1337' AND FirstName='42';").Rows[0][1], 80);
        }

        [TestMethod]
        public void DeleteCharact()
        {
            MyDal.DeleteCharacterInBDD("42","1337");
            MyDal.DeleteCharacterInBDD("très", "Null");
            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Characters;").Rows.Count, 0);
        }
        
        [TestMethod]
        public void DeleteTerritory()
        {
            DataTable dt = MyDal.SelectByDataAdapter("SELECT IdTerritory FROM dbo.Territories;");
            MyDal.DeleteTerritoryInBDD(int.Parse(dt.Rows[0][0].ToString()));
            MyDal.DeleteTerritoryInBDD(int.Parse(dt.Rows[1][0].ToString()));
            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Territories;").Rows.Count, 0);
        }
        [TestMethod]
        public void DeleteFight()
        {
            DataTable dt = MyDal.SelectByDataAdapter("SELECT IdFight FROM dbo.Fights;");
            MyDal.DeleteFightInBDD(int.Parse(dt.Rows[0][0].ToString()));
            MyDal.DeleteFightInBDD(int.Parse(dt.Rows[1][0].ToString()));
            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Fights;").Rows.Count, 0);
        }
        [TestMethod]
        public void DeleteHouse()
        {
            MyDal.DeleteHouseInBDD("Leet");
            MyDal.DeleteHouseInBDD("Zero");
            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Houses;").Rows.Count, 0);
        }
        [TestMethod]
        public void DeleteWar()
        {
            DataTable dt = MyDal.SelectByDataAdapter("SELECT Id FROM dbo.Wars;");
            MyDal.DeleteWarInBDD(int.Parse(dt.Rows[0][0].ToString()));
            MyDal.DeleteWarInBDD(int.Parse(dt.Rows[1][0].ToString()));
            Assert.AreEqual(MyDal.SelectByDataAdapter("SELECT * FROM dbo.Wars;").Rows.Count, 0);
        }
    }
}
