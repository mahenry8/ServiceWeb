﻿CREATE TABLE [dbo].[Relationships]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [IdPerso1] INT NOT NULL, 
    [IdPerso2] INT NOT NULL, 
    [Relationship] VARCHAR(50) NOT NULL, 
    CONSTRAINT [IdPerso1] FOREIGN KEY ([IdPerso1]) REFERENCES [Characters]([IdCharacter]), 
    CONSTRAINT [IdPerso2] FOREIGN KEY ([IdPerso2]) REFERENCES [Characters]([IdCharacter])
)
