﻿CREATE TABLE [dbo].[Houses]
(
	[Name] VARCHAR(50) NOT NULL PRIMARY KEY, 
    [NumberOfUnities] INT NULL
)
