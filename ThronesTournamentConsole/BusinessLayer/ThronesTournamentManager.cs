﻿using DataAccessLayer;
using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    class ThronesTournamentManager
    {
        public DalManager dal;
     
        
        
        public ThronesTournamentManager(DalManager d)
        {
            dal = d;
        }
        
        public String getHouses()
        {
            String result="";
            List<EntitiesLayer.House> Houses = dal.ExistingHouses();
            foreach(House h in Houses)
            {
                result += h.Name + " ";
            }
            return result;
        }

        public String getHousesSup200U()
        {
            String result = "";
            List<EntitiesLayer.House> Houses = dal.ExistingHouses();
            var selectedHouses = from h in Houses where h.NumberOfUnities > 200 select h.Name;
            foreach (String n in selectedHouses)
            {
                result += n + " ";
            }
            return result;
        }

        public String getStrongCharacters()
        {
            String result = "";
            List<EntitiesLayer.Character> Characters = dal.ExistingCharacters();
            var selectedCharacters = from c in Characters where c.Bravoury > 3 && c.Pv >50 select c.FirstName + " " + c.LastName;
            foreach (String n in selectedCharacters)
            {
                result += n + " ";
            }
            return result;
        }

         public String getTerritories()
        {
            String result = "";
            List<EntitiesLayer.Territory> Territories = dal.ExistingTerritories();
            foreach (Territory t in Territories)
            {
                result += t.Type + " ";
            }
            return result;
        }


    }
}
