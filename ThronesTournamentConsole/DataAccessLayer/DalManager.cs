﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using System.Data;
using System.Data.SqlClient;


namespace DataAccessLayer
{
    public class DalManager
    {
        private static DalManager instance;
        private string connexionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Cours\\ServiceWeb\\ThronesTournamentConsole\\BDD\\Data.mdf;Integrated Security=True;Connect Timeout=30";

        private DalManager() {}

        public static DalManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DalManager();
                }
                return instance;
            }
        }


        public DataTable SelectByDataAdapter (string request)
        {
            DataTable results = new DataTable();
            SqlConnection sqlConnection;
            using (sqlConnection = new SqlConnection(connexionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter reader;

                cmd.CommandText = request;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;
                //sqlConnection1.Open();

                reader = new SqlDataAdapter(cmd);
                reader.Fill(results);

                //sqlConnection1.Close();
            }
                

            return results;
        }

        public House GetHouseByName(String n)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE Name='" + n + "';");
            DataRow ligne = dt.Rows[0];
            House h = new House(ligne["Name"].ToString(), int.Parse(ligne["NumberOfUnities"].ToString()));
            return h;
        }
        public List<EntitiesLayer.House> ExistingHouses()
        {
            List<House> ExistingH = new List<House>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses;");

            List<Character> charac = ExistingCharacters();

            foreach (DataRow ligne in dt.Rows)
            {
                House h = new House(ligne["Name"].ToString());
                h.NumberOfUnities = int.Parse(ligne["NumberOfUnities"].ToString());
                h.Housers = charac.FindAll(c => c.HouseName == h.Name);
                ExistingH.Add(h);
            }

            return ExistingH;
        }

        public List<EntitiesLayer.House> getBigHouses(List<EntitiesLayer.House> ExistingHouses)
        {
            List<House> ExistingH = new List<House>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE NumberOfUnities >= 200;");

            List<Character> charac = ExistingCharacters();

            foreach (DataRow ligne in dt.Rows)
            {
                House h = new House(ligne["Name"].ToString());
                h.NumberOfUnities = int.Parse(ligne["NumberOfUnities"].ToString());
                h.Housers = charac.FindAll(c => c.HouseName == h.Name);
                ExistingH.Add(h);
            }

            return ExistingH;
        }

        public War GetWarById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars WHERE Id =" + id.ToString() + ";");
            DataRow ligne = dt.Rows[0];
            War w = new War(int.Parse(ligne["Id"].ToString()), ligne["Name"].ToString());
            return w;
        }

        public List<EntitiesLayer.War> ExistingWars()
        {
            List<War> ExistingW = new List<War>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars;");
            List<Fight> fights = ExistingFights();
            foreach (DataRow ligne in dt.Rows)
            {
                War w = new War(int.Parse(ligne["Id"].ToString()),ligne["Name"].ToString());
                w.combats = fights.FindAll(f => f.idWar == w.getId());
                ExistingW.Add(w);  
            }
            return ExistingW;
        }

        public Fight GetFightById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights WHERE IdFight =" + id.ToString() +";");
            DataRow ligne = dt.Rows[0];
            Fight f = new Fight(ligne["HouseName1"].ToString(), ligne["HouseName2"].ToString(), int.Parse(ligne["idWar"].ToString()), int.Parse(ligne["IdFight"].ToString()));
            f.winningHouse = ligne["WinningHouse"].ToString();
            return f;
        }

        public List<EntitiesLayer.Fight> ExistingFights()
        {
            List<Fight> ExistingFights = new List<Fight>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");

            foreach (DataRow ligne in dt.Rows)
            {
                Fight f = new Fight(ligne["HouseName1"].ToString(), ligne["HouseName2"].ToString(), int.Parse(ligne["idWar"].ToString()), int.Parse(ligne["IdFight"].ToString()));
                f.winningHouse = ligne["WinningHouse"].ToString();
                ExistingFights.Add(f);
            }
            return ExistingFights;
        }

        public Territory GetTerritoryById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories WHERE IdTerritory = "+ id.ToString() +";");
            DataRow ligne = dt.Rows[0];
            Territory t = new Territory((TerritoryType)Enum.Parse(typeof(TerritoryType), ligne["Type"].ToString()), ligne["Owner"].ToString(), int.Parse(ligne["IdTerritory"].ToString()));
            return t;
        }

        public List<Territory> ExistingTerritories()
        {
            List<Territory> ExistingT = new List<Territory>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");

            foreach (DataRow ligne in dt.Rows)
            {
                Territory t = new Territory((TerritoryType)Enum.Parse(typeof(TerritoryType), ligne["Type"].ToString()), ligne["Owner"].ToString(), int.Parse(ligne["IdTerritory"].ToString()));
                ExistingT.Add(t);
            }
            return ExistingT;
        }

        public EntitiesLayer.Character GetCharacterById(int id)
        {
            List<Character> ExistingC = ExistingCharacters();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters WHERE IdCharacter = " + id.ToString() + ";");
            DataRow ligne = dt.Rows[0];
            Character c = new Character(int.Parse(ligne["IdCharacter"].ToString()), int.Parse(ligne["Bravoury"].ToString()), int.Parse(ligne["Crazyness"].ToString()), ligne["FirstName"].ToString(), ligne["LastName"].ToString(), int.Parse(ligne["Pv"].ToString()), ligne["HouseName"].ToString(), (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), ligne["CharacterType"].ToString()));
            DataTable R = SelectByDataAdapter("SELECT * FROM dbo.Relationships WHERE IdPerso1 =" + c.id.ToString() + ";");
            foreach (DataRow relation in R.Rows)
            {
                c.AddRelatives((RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), relation["Relationship"].ToString()), ExistingC.Find(x => x.id == int.Parse(relation["IdPerso2"].ToString())));
            }
            return c;
        }

        public List<EntitiesLayer.Character> ExistingCharacters()
        {
            List<Character> ExistingC = new List<Character>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters;");

            foreach (DataRow ligne in dt.Rows)
            {
                Character c = new Character(int.Parse(ligne["IdCharacter"].ToString()), int.Parse(ligne["Bravoury"].ToString()), int.Parse(ligne["Crazyness"].ToString()), ligne["FirstName"].ToString(), ligne["LastName"].ToString(), int.Parse(ligne["Pv"].ToString()), ligne["HouseName"].ToString(), (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), ligne["CharacterType"].ToString()));
                ExistingC.Add(c);
            }
            foreach (Character c in ExistingC)
            {
                DataTable R = SelectByDataAdapter("SELECT * FROM dbo.Relationships WHERE IdPerso1 =" + c.id.ToString() + ";");
                foreach (DataRow relation in R.Rows)
                {
                    c.AddRelatives((RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), relation["Relationship"].ToString()), ExistingC.Find(x => x.id == int.Parse(relation["IdPerso2"].ToString()) ));
                }
            }

            return ExistingC;
        }


        private int UpdateByCommandBuilder(String request, DataTable authors)
        {
            int result = 0;
            SqlConnection sqlConnection;
            using (sqlConnection = new SqlConnection(connexionString))
            {
                SqlCommand sqlCommand = new SqlCommand(request, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();

                sqlDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                result = sqlDataAdapter.Update(authors);
            }
            return result;
        }

        public void AddCharacterInBDD(Character charac)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Characters;");
            DataRow myRow = dt.NewRow();
            myRow[1] = charac.Bravoury.ToString();
            myRow[2] = charac.Crazyness.ToString();
            myRow[3] = charac.FirstName;
            myRow[4] = charac.LastName;
            myRow[5] = charac.Pv.ToString();
            myRow[6] = charac.HouseName;
            myRow[7] = charac.CharacterType.ToString();
            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Characters;", dt);
        }
        public void AddHouseInBDD(House h)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Houses;");
            DataRow myRow = dt.NewRow();
            myRow[0] = h.Name;
            myRow[1] = h.NumberOfUnities.ToString();
            dt.Rows.Add(myRow);
            UpdateByCommandBuilder("Select * FROM dbo.Houses;", dt);
        }

        public void AddTerritoryInBDD(Territory t)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Territories;");
            DataRow myRow = dt.NewRow();
            myRow[1] = t.Type.ToString();
            myRow[2] = t.Owner;

            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Territories;", dt);
        }
        public void AddWarInBDD(War w)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Wars;");
            DataRow myRow = dt.NewRow();
            myRow[1] = w.name;
           
            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Wars;", dt);
        }
        public void AddFightInBDD(Fight f)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Fights;");
            DataRow myRow = dt.NewRow();
            myRow[1] = f.houseChallenger1;
            myRow[2] = f.houseChallenger2;
            myRow[3] = f.winningHouse;
            myRow[4] = f.idWar;
            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Fights;", dt);
        }

        public void DeleteCharacterInBDD(String fn, String ln)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Characters;");

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (row["FirstName"].ToString() == fn && row["LastName"].ToString() == ln )
                    row.Delete();
            }
            UpdateByCommandBuilder("Select * FROM dbo.Characters;", dt);
        }

        public void DeleteHouseInBDD(String name)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (row["Name"].ToString() == name)
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Houses;", dt);
        }

        public void DeleteTerritoryInBDD(int id_t)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["IdTerritory"].ToString()) == id_t)
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Territories;", dt);
        }

        public void DeleteFightInBDD(int id_f)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["IdFight"].ToString()) == id_f)
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Fights;", dt);
        }
        public void DeleteWarInBDD(int id_w)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["Id"].ToString()) == id_w)
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Wars;", dt);
        }
        public void UpdateCharacterInBDD(Character charac)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters;");
            foreach(DataRow myRow in dt.Rows)
            {
                if(myRow[3].ToString()==charac.FirstName && myRow[4].ToString() == charac.LastName)
                {
                    myRow[1] = charac.Bravoury.ToString();
                    myRow[2] = charac.Crazyness.ToString();
                    myRow[5] = charac.Pv.ToString();
                    myRow[6] = charac.HouseName;
                    myRow[7] = charac.CharacterType.ToString();
                }
            }

            UpdateByCommandBuilder("SELECT * FROM dbo.Characters;", dt);
        }
        public void UpdateHouseInBDD(House h)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (myRow[0].ToString() == h.Name)
                {
                    myRow[1] = h.NumberOfUnities.ToString();
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Houses;", dt);
        }

        public void UpdateTerritoryInBDD(Territory t)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow[0].ToString()) == t.id)
                {
                    myRow[1] = t.Type.ToString();
                    myRow[2] = t.Owner;
                }

            }
                UpdateByCommandBuilder("SELECT * FROM dbo.Territories;", dt);
        }

        public void UpdateFightInBDD(Fight f)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow[0].ToString()) == f.id)
                {
                    myRow[1] = f.houseChallenger1;
                    myRow[2] = f.houseChallenger2;
                    myRow[3] = f.winningHouse;
                    myRow[4] = f.idWar;
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Fights;", dt);
        }

        public void UpdateWarInBDD(War w)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow[0].ToString()) == w.getId())
                {
                    myRow[1] = w.name;
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Wars;", dt);
        }


    }
}
