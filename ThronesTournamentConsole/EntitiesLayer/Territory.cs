﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Territory : EntityObject
    {
        public TerritoryType Type { get; set; }
        public String Owner { get; set; }
        public int id { get; set; }

        public Territory(TerritoryType t, String o, int i)
        {
            Type = t;
            Owner = o;
            id = i;
        }
    }
}
