﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class War : EntityObject
    {
        public int id { get; set; }
        public List<Fight> combats { get; set; }
        public String name { get; set; }

        public War(int i, String n)
        {
            id = i;
            name = n;
        }

        public void AddFight(Fight f)
        {
            combats.Add(f);
        }
    }
}
