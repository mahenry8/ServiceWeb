﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Character : EntityObject
    {
        public int id { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public int Pv { get; set; }
        public String HouseName { get; set; }
        public List<Tuple<RelationshipEnum, Character>> Relationships { get; set; }
        public CharacterTypeEnum CharacterType { get; set; }

        public void AddRelatives(RelationshipEnum relation, Character c)
        {
            Tuple<RelationshipEnum, Character> tmp = new Tuple<RelationshipEnum, Character>(relation, c);
            Relationships.Add(tmp);
        }
        public Character(int i, int brav, int craz, String firstn, String lastn, int p, String hn,CharacterTypeEnum t)
        {
            id = i;
            Bravoury = brav;
            Crazyness = craz;
            FirstName = firstn;
            LastName = lastn;
            Pv = p;
            HouseName = hn;
            Relationships = new List<Tuple<RelationshipEnum, Character>>();
            CharacterType = t;
        }

        override
        public String ToString()
        {
            String result = FirstName + " " + LastName + " a " + Bravoury + " de bravoure, " + Crazyness + " de folie " + Pv + " de Pv, ";
            foreach (Tuple<RelationshipEnum, Character> s in Relationships)
                result += " et a la relation : " + s.Item1 + " avec " + s.Item2.FirstName +" "+ s.Item2.LastName;
            return result;
        }
    }
}
