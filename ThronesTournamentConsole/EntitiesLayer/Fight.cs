﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Fight : EntityObject
    {
        public String houseChallenger1 { get; set; }
        public String houseChallenger2 { get; set; }
        public String winningHouse { get; set; }
        public int idWar { get; set; }
        public int id { get; set; }

        public Fight(String h1, String h2, int iw, int i)
        {
            houseChallenger1 = h1;
            houseChallenger2 = h2;
            winningHouse = null;
            idWar = iw;
            id = i;
        }

    }
}
