﻿/*$(document).ready(function () {
    
});*/
function getHousers(house) {
   /* xhr = $.ajax({
        url: url_api("Houses/Housers/"+house),
        dataType: 'json',
        type: "GET"
    }).done(function (houses) {
        affiche_characters(characters);
    });
    utiliser xhr.abort() pour faire des requetes propres
    */
    affiche_characters(characters);
}
function affiche_characters(characters) {
    $('#characters').html("");
    for (var i in characters) {
        character = characters[i];
        $("#characters").append('\
            <div class="col-md-4">\
                <h2>'+ character.FirstName +" "+ character.LastName + '</h2>\
                <p>\
                    Bravoury: ' + character.Bravoury + '<br />\
                    Crazyness:' + character.Crazyness + '<br />\
                    PV:' + character.PV + '\
                </p>\
            </div>\
            ').hide().fadeIn();
    }
}
function affiche_houses(houses) {
    for (var i in houses) {
        house = houses[i];
        if (window.location.hash == "#" +house.name) active = "active";
        else active = "";
        $("#houses").append('\
            <a href="#'+ house.name +'" class="list-group-item '+active+' housesSelect" >'+house.name+'</a>\
        ');
    }
    /*onclick="getHousers(\''+house+'\')"*/
    $('.housesSelect').on("click", function (event) {
        getHousers($(this).html());
        $('.housesSelect').removeClass("active");
        $(this).addClass("active");
    });
}


/*
<a class="dropdown-item" href="#">Regular link</a>
            <a class="dropdown-item active" href="#">Active link</a>
            <a class="dropdown-item" href="#">Another link</a>


*/
var characters = [
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu2", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu3", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu4", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu5", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu6", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu7", LastName: "Moi", PV: 42 },
    { Bravoury: 1, Crazyness: 2, FirstName: "Dieu8", LastName: "Moi", PV: 42 }
]

houses = [
    { name: "Maison1", NumberOfUnities: 4 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison3", NumberOfUnities: 42 },
    { name: "Maison4", NumberOfUnities: 42 },
    { name: "Maison5", NumberOfUnities: 42 }
]
if (window.location.hash != "") affiche_characters(characters);
affiche_houses(houses) 