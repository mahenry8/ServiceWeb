﻿$(document).ready(function () {
    $('.fighter').click(function (event) {
        attack(this, "#f3", 42);
    });
})

function animate_pv_lost(pv, element) {
    $('#lifelost').remove();
    $(element).append("<div id='lifelost'>-" + pv + "</div>");
    $(element).children("progress").val($(element).children("progress").val()-pv);
    $('#lifelost').animate({
        top: 0,
        opacity: 0
    }, 800);

    if ($(element).children("progress").val() <= 0) {
        $(element).hide("explode", function () {

            $(element).children("img").remove();
            $(element).append("<img src='https://cdn.shopify.com/s/files/1/0683/3125/products/2661569603-white-halloween-skull-and-crossbones-wall-decal-sticker-weedecor.jpg?v=1442971760' />");

        });
        $(element).show("explode", function () { $(this).css({ "opacity": "0.4" }) });
    }
}

function attack(attaquant, victime, PV) {
    $("#fireball").remove();
    $("body").append("<div id='fireball'><img src='/Content/fire.png' /></div>");
    $('#fireball').css({
        "position": "absolute",
        top: $(attaquant).offset().top + $(attaquant).width() / 2,
        left: $(attaquant).offset().left + $(attaquant).height() / 2,
        width: 48
    });
    $('#fireball').addClass("launched");
    $('#fireball').css({
        "position": "absolute",
        top: $(victime).offset().top + $(victime).width() / 2,
        left: $(victime).offset().left + $(victime).height() / 2,
    });
    $('#fireball img').delay(500).hide("explode");
    setTimeout(function () { animate_pv_lost(PV, victime) }, 500);
}

function lose_unities(cote, nombre) {
    i = 0;
    $('#unit' + cote + " .unit").each(function () {
        if (i++ < nombre) $(this).remove();
    });
}