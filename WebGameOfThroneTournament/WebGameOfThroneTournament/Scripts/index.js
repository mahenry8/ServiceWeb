﻿/*$(document).ready(function () {
    $.ajax({
        url: url_api("Houses"),
        dataType: 'json',
        type: "GET"
    }).done(function (houses) {
        affiche_houses(houses);
    });
});*/

function affiche_houses(houses) {
    for (var i in houses) {
        house = houses[i];
        $("#houses").append('\
            <div class="col-md-4">\
                <h2>'+ house.name + '</h2>\
                <p>\
                    Number of unities: <span id="'+house.name+'NoU">'+ house.NumberOfUnities +'</span><br />\
                    <a data-toggle="modal" data-target="#ModalEditHouse" onclick="form_edit_house(\''+ house.name +'\')">Edit house</a><br />\
                    <a data-toggle="modal" data-target="#AddHouser" onclick="form_add_houser(\''+ house.name +'\')">Add Housers</a><br />\
                    <a href="/Home/Housers#'+house.name+'">See Housers</a><br />\
                    <a>Choose this house</a><br />\
                </p>\
            </div>\
            ');
    }
}

function form_edit_house(name) {
    $('#HouseNameForm').html(name);
    $('#HouseNumberOfUnities').val($('#' + name + 'NoU').html());
}
function form_add_houser(name) {
    $('#AddHouserHouseName').val(name);
}

/*A ENLEVER QUAND ON A LA VRAIE API*/
houses = [
    { name: "Maison1", NumberOfUnities: 4 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 },
    { name: "Maison2", NumberOfUnities: 42 }
]

affiche_houses(houses);